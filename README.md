# Twitchmacs
An emacs mode for watching twitch videos with livestreamer and chatting through emacs erc.

## Requirements
To open streams in VLC make sure both VLC and livestreamer are installed.

To open Twitch chat through erc you need to generate an OAuth Token for your account. You can do this by visiting [this url](https://twitchapps.com/tmi/).
You should then set the `twitchmacs-erc-nick` and `twitchmacs-erc-oauth` vars.

## Usage
You can call twitchmacs through `M-x twitchmacs`.

You can set the variables `twitchmacs-browser`, `twitchmacs-default-play-quality`, `twitchmacs-twitch-api-limit` and the alist `twitchmacs-games-abbrev-list` for game abbreviations when calling `twitchmacs-streams`.
