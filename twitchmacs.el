;;; twitchmacs-mode.el --- sample major mode for interacting with twitchmacs-output buffers.

;; Copyright © 2017, one-zero

;; Author: one-zero ( myspqmhq@gmail.com )
;; Version: 0.1.0
;; Created: 30/06/2017
;; Keywords: twitch.tv twitch
;; URL: https://gitlab.com/one-zero/twitchmacs

;; This file is not part of GNU Emacs.

;;; License: GPLv3

;; You can redistribute this program and/or modify it under the terms of the GNU General Public License version 3.

;;; Commentary:

;; short description here

;; full doc on how to use here


;;; Code:


(require 'json)
(require 'cl)

;; Check why defvar way doesn't work
(progn
  (setq twitchmacs-mode-map (make-sparse-keymap))
  (define-key twitchmacs-mode-map (kbd "C-c C-s") 'twitchmacs-open-chat-erc)
  (define-key twitchmacs-mode-map (kbd "C-c C-d") 'twitchmacs-games)
  (define-key twitchmacs-mode-map (kbd "C-c C-c") 'twitchmacs-play-stream)
  (define-key twitchmacs-mode-map (kbd "C-c C-g") 'twitchmacs-open-chat-browser)
  )

(easy-menu-define twitchmacs-menu twitchmacs-mode-map
  "Twitchmacs Mode Commands"
  '("Twitchmacs"
    ["Show streams for the selected game" twitchmacs-list-channels-of-game]))

;; define several category of keywords
(setq twitchmacs-keywords '("Url" "Title" "Name" "Viewers"))


(setq twitchmacs-keywords-regexp (regexp-opt twitchmacs-keywords 'words))

(setq twitchmacs-font-lock-keywords
      `(
        (,twitchmacs-keywords-regexp . font-lock-keyword-face)
        ))

;; Menu functions:

(defun twitchmaps-remap-clear-menu ()
  "Clear menu entries"
  (easy-menu-remove-item nil '("Twitchmacs") "Show streams for the selected game")
  (easy-menu-remove-item nil '("Twitchmacs") "Play stream")
  (easy-menu-remove-item nil '("Twitchmacs") "Open chat in erc")
  (easy-menu-remove-item nil '("Twitchmacs") "Open chat in browser")
  (easy-menu-remove-item nil '("Twitchmacs") "List games by viewers")
  )

(defun twitchmacs-channels-remap ()
  "Populate menu for channel viewing"
  (twitchmaps-remap-clear-menu)
  (define-key twitchmacs-mode-map (kbd "C-c C-c") 'twitchmacs-play-stream)
  (easy-menu-add-item nil '("Twitchmacs") ["Play stream" twitchmacs-play-stream])
  (easy-menu-add-item nil '("Twitchmacs") ["Open chat in erc" twitchmacs-open-chat-erc])
  (easy-menu-add-item nil '("Twitchmacs") ["Open chat in browser" twitchmacs-open-chat-browser])
  (easy-menu-add-item nil '("Twitchmacs") ["List games by viewers" twitchmacs-games])
  
  )

(defun twitchmacs-games-remap ()
  "Populate menu for game viewing"
  (twitchmaps-remap-clear-menu)
  (define-key twitchmacs-mode-map (kbd "C-c C-c") 'twitchmacs-list-channels-of-game)
  (easy-menu-add-item nil '("Twitchmacs") ["Show streams for the selected game" twitchmacs-list-channels-of-game])
  )

;; Menu functions end
  
   
(defun twitchmacs-list-channels-of-game ()
  "Lists live channels of the selected game sorted by viewers"
  (interactive)
  (let* ((char-at-position (string (char-after)))
        (args
         (cond
          ((string= char-at-position "(")
           (search-forward "Name: ")
           (buffer-substring (point) (line-end-position)))
          (t
           (search-backward "(")
           (search-forward "Name: ")
           (buffer-substring (point) (line-end-position))))))
    (twitchmacs-streams args)
    ))


(defun twitchmacs-play-stream (quality)
  "Opens the selected stream with livestreamer"
  (interactive
   (list
    (read-string (concat "Stream quality? (default is '" twitchmacs-default-play-quality "') "))))
  (start-process-shell-command "twitchmacs" "*Messages*"
   (concat
    "livestreamer --http-header Client-ID=jzkbprff40iqj646a697cyrvl0zt2m6 "
    (let ((char-at-position (string (char-after))))
      (cond
       ((string= char-at-position "(")
        (search-forward "Url: ")
        (buffer-substring (point) (line-end-position)))
       (t
        (search-backward "(")
        (search-forward "Url: ")
        (buffer-substring (point) (line-end-position)))))
    (if (string= quality "")
        (concat " " twitchmacs-default-play-quality)
      (concat " " quality)))))


;; uses vars twitchmacs-browser and twitchmacs-browser-extra-args
(defun twitchmacs-open-chat-browser ()
  "Opens the selected streams chat in the browser"
  (interactive)
  (call-process-shell-command
   (concat
    "\"" twitchmacs-browser "\" \""
    (let ((char-at-position (string (char-after))))
      (cond
       ((string= char-at-position "(")
        (search-forward "Url: ")
        (buffer-substring (point) (line-end-position)))
       (t
        (search-backward "(")
        (search-forward "Url: ")
        (buffer-substring (point) (line-end-position)))))
    "/chat?popout=\""
    (when twitchmacs-browser-extra-args
      twitchmacs-browser-extra-args))
   nil 0))

(defun twitchmacs-open-chat-erc ()
  "Connect to twitch chat through erc"
  (interactive)
  (let ((channel
         (cond
          ((string= (string (char-after)) "(")
           (search-forward "Name: ")
           (buffer-substring (point) (line-end-position)))
          (t
           (search-backward "(")
           (search-forward "Name: ")
           (buffer-substring (point) (line-end-position))))))
    (if (and twitchmacs-erc-nick twitchmacs-erc-oauth)
        (progn
          (erc :server "irc.twitch.tv" :port 6667 :nick twitchmacs-erc-nick :password twitchmacs-erc-oauth)
          (make-local-variable 'erc-autojoin-channels-alist)
          (make-local-variable 'erc-autojoin-timing)
          (setq erc-autojoin-timing 'connect)
          (setq erc-autojoin-channels-alist `((".*" ,(concat "#" (downcase channel))))))
      (message "You need to set the twitchmacs-erc-nick and twitchmacs-erc-oauth variables"))))

(defun twitchmacs-games ()
  "Lists all the running streams by search criteria given as an associative list"
  (interactive)
  (let ((streams
         (twitchmacs-games-formatter
          (twitchmacs-response-to-json
           (twitchmacs-twitch-api-call
            "https://api.twitch.tv/kraken/games/top" (list (cons "limit" twitchmacs-twitch-api-limit))) "games"))))
    (when (get-buffer "twitchmacs-output")
      (kill-buffer "twitchmacs-output"))
    (with-current-buffer (switch-to-buffer (generate-new-buffer "twitchmacs-output"))
      (insert (format "%s" (twitchmacs-games-print-formatter streams)))
      (twitchmacs-mode)
      (goto-char (point-min))
      (read-only-mode)
      (twitchmacs-games-remap)
      (message (concat "Most popular " twitchmacs-twitch-api-limit " games")))))

(defun twitchmacs-streams (game)
  "Lists all the running streams by search criteria given as an associative list"
  (interactive
   (list
    (read-string "Game: ")))
  (let ((streams
         (twitchmacs-streams-formatter
          (twitchmacs-response-to-json
           (twitchmacs-twitch-api-call
            "https://api.twitch.tv/kraken/streams" (list (cons "game" (twitchmacs-decode-game-name game)) (cons "limit" twitchmacs-twitch-api-limit))) "streams"))))
    (when (get-buffer "twitchmacs-output")
      (kill-buffer "twitchmacs-output"))
    (with-current-buffer (switch-to-buffer (generate-new-buffer "twitchmacs-output"))
      (insert (format "%s" (twitchmacs-streams-print-formatter streams)))
      (twitchmacs-mode)
      (goto-char (point-min))
      (read-only-mode)
      (twitchmacs-channels-remap)
      (message "%s" (twitchmacs-decode-game-name game)))))

(defun twitchmacs-decode-game-name (game)
  "Decodes the shortened game name into a hexified value"
    (if (assoc game twitchmacs-games-abbrev-list)
        (cdr (assoc game twitchmacs-games-abbrev-list))
      game))


  
(defun twitchmacs-lister (args)
  "Lists all the running streams by search criteria given as an associative list"
  (let ((streams (twitchmacs-streams-formatter (twitchmacs-response-to-json (twitchmacs-twitch-api-call "https://api.twitch.tv/kraken/streams" args)))))
    (with-current-buffer (switch-to-buffer (generate-new-buffer "twitchmacs-output"))
      (insert (format "%s" (twitchmacs-streams-print-formatter streams)))
      (goto-char (point-min))
      (message "%s" args))))
                 

(defun twitchmacs-twitch-api-call (url args)
  "Send ARGS to URL as a GET request.
Decode the response to utf-8 with twitchmacs-recode-string"
  (let ((url-request-method "GET")
        (url-request-extra-headers
         '(("Accept" . "application/vnd.twitchtv.v3+json")
           ("Client-Id" . "ct6ljicu3b32f6y34vxz9dqmcppv8g")))
        (arg-string
         (mapconcat (lambda (arg)
                      (concat (url-hexify-string (car arg))
                              "="
                              (url-hexify-string (cdr arg))))
                    args
                    "&")))
    (with-current-buffer (switch-to-buffer (url-retrieve-synchronously (concat url "?" arg-string)))
      (let
          ((raw-response (buffer-string)))
        (kill-buffer)
        (twitchmacs-recode-string raw-response)))))

(defun twitchmacs-response-to-json (response type)
  "Strip the header from the API response and encode the result into a property list"
  (let ((just-json (substring response (string-match "\{" response)))
        (json-object-type 'plist))               ;;':top
    (cond
     ((string= type "streams")
      (plist-get (json-read-from-string just-json) ':streams))
     ((string= type "games")
      (plist-get (json-read-from-string just-json) ':top)))))

(defun twitchmacs-games-formatter (vector)
  "Filters the received vector containing a list of stream objects into a property list"
  (mapcar
   (lambda (x)
     (list
      :name (plist-get (plist-get x ':game) ':name)
      :viewers (plist-get (plist-get x ':game) ':popularity)
      ))
   vector))

(defun twitchmacs-streams-formatter (vector)
  "Filters the received vector containing a list of stream objects into a property list"
  (mapcar
   (lambda (x)
     (list
      :name (plist-get (plist-get x ':channel) ':display_name)
      :viewers (plist-get x ':viewers)
      :title (plist-get (plist-get x ':channel) ':status)
      :url (plist-get (plist-get x ':channel) ':url)
      ))
   vector))


(defun twitchmacs-games-print-formatter (streams)
  "Formats the filtered stream property list for printing in the output buffer"
  (mapconcat
   (lambda (x)
     (concat
      "(\n"
      "Name: " (format "%s" (plist-get x ':name)) "\n"
      "Viewers: " (format "%s" (plist-get x ':viewers)) "\n"
      ")\n"
      ))
   streams ""))

(defun twitchmacs-streams-print-formatter (streams)
  "Formats the filtered stream property list for printing in the output buffer"
  (mapconcat
   (lambda (x)
     (concat
      "(\n"
      "Name: " (format "%s" (plist-get x ':name)) "\n"
      "Viewers: " (format "%s" (plist-get x ':viewers)) "\n"
      "Title: " (format "%s" (plist-get x ':title)) "\n"
      "Url: " (format "%s" (plist-get x ':url)) "\n"
      ")\n"
      ))
   streams ""))


(defun recode-buffer ()
  "Recodes current buffer into utf-8"
  (interactive)
  (recode-region (point-min) (point-max) 'utf-8 'raw-text))

(defun twitchmacs-recode-string (string)
  "Recodes the recieved string into utf-8"
  (with-temp-buffer
    (insert (format "%s" string))
    (recode-region (point-min) (point-max) 'utf-8 'raw-text)
    (buffer-string)))

(defun twitchmacs ()
  "Calls twitchmacs-games"
  (interactive)
  (twitchmacs-games))

(defvar twitchmacs-twitch-api-limit
  "30"
  "Number of objects to return per api request")

(defvar twitchmacs-default-play-quality
  "best"
  "The default quality for opening a stream")

(defvar twitchmacs-browser
  "firefox"
  "Path to default browser")

(defvar twitchmacs-browser-extra-args
  nil
  "Extra arguments to supply for running the browser")

(defvar twitchmacs-erc-nick
  nil
  "Used for connecting to twitch chat through erc")

(defvar twitchmacs-erc-oauth
  nil
  "Used for connecting to twitch chat through erc")

(defvar twitchmacs-games-abbrev-list
  '(
    ("csgo" . "Counter-Strike: Global Offensive")
    ("dota2" . "Dota 2")
    ("lol". "League of Legends")
    ("hs" . "Hearthstone")
    ("dota" . "Dota 2")
    ("doto" . "Dota 2")
    )
  "List of abbreviations that can be used for game names")

;;;###autoload
(define-derived-mode twitchmacs-mode fundamental-mode "twitchmacs mode"
  "Major mode for interacting with twitchmacs output"

  (use-local-map twitchmacs-mode-map)
  (easy-menu-add twitchmacs-menu)
  
  (setq font-lock-defaults '((twitchmacs-font-lock-keywords))))



;; clear memory. no longer needed
(setq twitchmacs-keywords nil)
(setq twitchmacs-keyword-regexp nil)


;; add the mode to the `features' list
(provide 'twitchmacs-mode)
